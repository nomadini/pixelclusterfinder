#ifndef ClustererExample1_H
#define ClustererExample1_H


#include "Status.h" //this is needed in every ClustererExample1

#include <memory>
#include <unordered_map>
#include <string>
#include <shogun/lib/common.h>
#define HAVE_LAPACK //this is needed for CGMM
class EntityToModuleStateStats;
class RandomUtil;

class ClustererExample1 {

private:

public:
std::shared_ptr<RandomUtil> randomizer;
std::shared_ptr<std::unordered_map<int, std::string> > clusterToRow;
ClustererExample1();

void addDataToMatrix(shogun::SGMatrix<float64_t>& f_feats_train);
virtual void process();
virtual void processGMMClusteringExample();
virtual void processHierarchicalClusteringExample();

virtual ~ClustererExample1();

};

#endif
