#include "ClustererExample1.h"
#include <shogun/clustering/Hierarchical.h>
#include "RandomUtil.h"
#include "ModelUtil.h"
#include "StringUtil.h"
#include "GUtil.h"

#include <shogun/distance/EuclideanDistance.h>
#include <shogun/features/SparseFeatures.h>
#include <shogun/lib/config.h>
#include <shogun/io/SGIO.h>
#include <shogun/clustering/GMM.h>


using namespace shogun;
ClustererExample1::ClustererExample1() {
        randomizer = std::make_shared<RandomUtil>(2);
}

void ClustererExample1::addDataToMatrix(SGMatrix<float64_t>& f_feats_train) {
        clusterToRow = std::make_shared<std::unordered_map<int, std::string> >();
        int dim_feat = 24;
        int num_samples = 1000;
        for (int32_t i = 0; i < num_samples; i++) {
                for (int32_t j = 0; j < dim_feat; j++) {
                        if (num_samples / 2 == 0) {
                                if (dim_feat / 2 == 0) {
                                        f_feats_train(j, i) = 1;
                                } else {
                                        f_feats_train(j, i) = 0;
                                }

                        } else {
                                f_feats_train(j, i) = randomizer->randomNumberV2();
                        }

                }
        }
        for (int32_t i = 0; i < num_samples; i++) {

                std::string rowStr;
                for (int32_t j = 0; j < dim_feat; j++) {
                        rowStr += _toStr(f_feats_train(j, i));
                }
                LOG(INFO)<<"sample "<<i<<": "<< rowStr;

                clusterToRow->insert(std::make_pair(i, rowStr));
        }


}

void ClustererExample1::process() {
        processHierarchicalClusteringExample();
        processGMMClusteringExample();
}

void ClustererExample1::processHierarchicalClusteringExample() {
        //I have not figured out how to interpret the results of this algorithm yet
        //I have not figured out how to interpret the results of this algorithm yet
        //I have not figured out how to interpret the results of this algorithm yet
        //I have not figured out how to interpret the results of this algorithm yet
        int dim_feat = 24;
        int num_samples = 1000;
        SGMatrix<float64_t> f_feats_train = SGMatrix<float64_t>(dim_feat, num_samples);

        addDataToMatrix(f_feats_train);

        CDenseFeatures<float64_t> *features_train =
                new CDenseFeatures<float64_t>(f_feats_train);
        auto distance = new CEuclideanDistance(features_train, features_train);
        auto merges = 10;
        auto hierarchical = new CHierarchical(merges, distance);
        auto successful = hierarchical->train();
        LOG(INFO)<<" training is successful ? " << successful;
        auto mergeDistances = hierarchical->get_merge_distances();
        LOG(INFO)<<"display_size ";
        mergeDistances.display_size ();
        LOG(INFO)<<"display_vector ";
        mergeDistances.display_vector("mergeDistances");
        auto matrixOfclusterPairs = hierarchical->get_cluster_pairs();
        matrixOfclusterPairs.display_matrix("matrixOfclusterPairs");

}

void ClustererExample1::processGMMClusteringExample() {

        int dim_feat = 24;
        int num_samples = 1000;
        SGMatrix<float64_t> f_feats_train = SGMatrix<float64_t>(dim_feat, num_samples);
        addDataToMatrix(f_feats_train);

        auto features_train = std::make_shared<CDenseFeatures<float64_t> > (f_feats_train);
        auto num_components = 10;
        auto gmm = std::make_shared<CGMM>(num_components);
        gmm->set_features(features_train.get());
        gmm->train_em(); //this is one way to train
        // gmm->train_smem(); or another way of training
        //we get a sample
        auto output = gmm->sample();

        //we choose the first cluster and get the means of features for that cluster
        auto component_num = 0;
        auto nth_mean = gmm->get_nth_mean(component_num);
        nth_mean.display_vector("first cluster nth_mean");

        auto nth_cov = gmm->get_nth_cov(component_num);
        nth_cov.display_matrix("first cluster nth_cov");

        auto coef = gmm->get_coef();
        coef.display_vector("coef");

        //here we get the array of log log_likelihoods of the first cluster means
        // to see if it correctly detects if our first cluster means belong to
        //first cluster or not. this cluster(feature) function returns n+1 values for the
        //response array where n is the number of clusters(components) and n+1th value determines
        //if the input features passed to cluster function is built by this model
        //we should find the maximum log returned by this array and that determines
        //which cluster our nth_mean belong to.
        auto log_likelihoods = gmm->cluster(nth_mean);
        log_likelihoods.display_vector("log_likelihoods");

        //we create a random vector here and we see that
        //the log log_likelihoods are all very into negatives for each cluster
        SGVector< float64_t > randomVector (24);
        randomVector.range_fill(0);
        for(int i = 0; i< 24; i++) {
                randomVector.set_element(i * 2, i);
        }

        auto log_likelihoods2 = gmm->cluster(randomVector);
        log_likelihoods2.display_vector("randomVector log_likelihoods");

}
ClustererExample1::~ClustererExample1() {
}
