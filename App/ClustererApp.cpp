
#include "GUtil.h"
#include "PixelDeviceHistoryCassandraService.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "PixelModelNegativeFeatureFetcher.h"
#include "MySqlOfferService.h"
#include "ClusterContext.h"
#include "DataFetcher.h"
#include "DataReloadService.h"

#include "LikelihoodToIabCatConverter.h"
#include "CassandraDriverInterface.h"
#include "ConfigService.h"
#include "TempUtil.h"
#include "MatrixPopulator.h"
#include "LogLevelManager.h"
#include <thread>
#include "GUtil.h"
#include "CacheService.h"
#include "StringUtil.h"
#include "SignalHandler.h"
#include <string>
#include <memory>
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "PixelClusterPersistenceModule.h"
#include "JsonMapUtil.h"
#include "ConfigService.h"
#include "StringUtil.h"
#include <string>

#include "ClusterIndexDescriber.h"
#include "EntityToModuleStateStatsPersistenceService.h"
#include "MySqlMetricService.h"
#include "ModelerAsyncJobService.h"
#include "FeatureBasedModeler.h"
#include "AtomicBoolean.h"
#include "ModelRequest.h"
#include "MySqlPixelService.h"
#include "FeatureFetcherService.h"
#include "Poco/NotificationQueue.h"
#include "Pixel.h"
#include "AsyncThreadPoolService.h"

#include <shogun/lib/config.h>
#include <shogun/base/init.h>
#include "BeanFactory.h"
#include "ModelRequest.h"
#include "ModelerPersistenceService.h"
#include "ModelDataCleaner.h"
#include "FeatureValidator.h"
#include "FeatureWeightToModelConverter.h"
#include "FeatureWeightCalculator.h"
#include "NegativeFeatureFetcher.h"
#include "DeviceFeatureHistory.h"
#include "PositiveFeatureFetcher.h"
#include "ServiceFactory.h"
#include "Clusterer.h"
#include "FeatureDeviceHistory.h"

int main(int argc, char* argv[]) {

        std::string appName = "pixelclusterfinder";
        TempUtil::configureLogging (appName, argv);
        std::string propertyFileName = "pixelclusterfinder.properties";
        auto beanFactory = std::make_unique<BeanFactory>("SNAPSHOT");
        beanFactory->propertyFileName = propertyFileName;
        beanFactory->commonPropertyFileName = "common.properties";
        beanFactory->appName = appName;
        beanFactory->initializeModules();
        auto serviceFactory = std::make_unique<ServiceFactory>(beanFactory.get());
        serviceFactory->initializeModules();


        //start of running clustere
        shogun::init_shogun_with_defaults();
        std::shared_ptr<ClusterContext> context = std::make_shared<ClusterContext>();
        auto clusterer = std::make_shared<Clusterer>();
        auto pixelClusterPersistenceModule = std::make_shared<PixelClusterPersistenceModule>();
        auto dataFetcher = std::make_shared<DataFetcher>();
        auto pixelClusterSelector = std::make_shared<PixelClusterSelector>();
        auto clusterIndexDescriber = std::make_shared<ClusterIndexDescriber>();
        auto matrixPopulator = std::make_shared<MatrixPopulator>();

        pixelClusterPersistenceModule->mySqlPixelClusterResultService =
                beanFactory->mySqlPixelClusterResultService.get();
        clusterer->pixelClusterPersistenceModule = pixelClusterPersistenceModule.get();

        auto likelihoodToIabCatConverter = std::make_shared<LikelihoodToIabCatConverter>();
        likelihoodToIabCatConverter->maxNumberOfHistoryPerDevice = beanFactory->configService->getAsInt("maxNumberOfHistoryPerDevice");
        likelihoodToIabCatConverter->featureRecencyInSecond = beanFactory->configService->getAsInt("featureRecencyInSecond");
        likelihoodToIabCatConverter->deviceFeatureHistoryCassandraService = beanFactory->deviceFeatureHistoryCassandraService.get();
        likelihoodToIabCatConverter->positiveValueInMatrix =
                beanFactory->configService->getAsDouble("positiveValueInMatrix");
        likelihoodToIabCatConverter->negativeValueInMatrix =
                beanFactory->configService->getAsDouble("negativeValueInMatrix");
        likelihoodToIabCatConverter->lowerThresholdForPickingCluster =
                beanFactory->configService->getAsDouble("lowerThresholdForPickingCluster");

        pixelClusterSelector->likelihoodToIabCatConverter =
                likelihoodToIabCatConverter.get();

        matrixPopulator->populatingStrategy =
                beanFactory->configService->get("populatingStrategy");

        matrixPopulator->positiveValueInMatrix =
                beanFactory->configService->getAsDouble("positiveValueInMatrix");
        matrixPopulator->negativeValueInMatrix =
                beanFactory->configService->getAsDouble("negativeValueInMatrix");

        dataFetcher->maxNumberOfIabCatsToPick = beanFactory->configService->getAsInt("maxNumberOfIabCatsToPick");
        dataFetcher->featureRecencyInSecond = beanFactory->configService->getAsInt("featureRecencyInSecond");
        dataFetcher->iabCatRecencyToPickInSecond = beanFactory->configService->getAsInt("iabCatRecencyToPickInSecond");
        dataFetcher->maxNumberOfHistoryPerDevice = beanFactory->configService->getAsInt("maxNumberOfHistoryPerDevice");
        dataFetcher->deviceFeatureHistoryCassandraService = beanFactory->deviceFeatureHistoryCassandraService.get();
        dataFetcher->featureDeviceHistoryCassandraService = beanFactory->featureDeviceHistoryCassandraService.get();
        dataFetcher->
        minTimesOfAppearanceForFeatureToConsiderForClustering =
                beanFactory->configService->getAsInt("minTimesOfAppearanceForFeatureToConsiderForClustering");

        pixelClusterSelector->minCountOfObservedAsTopCluster = beanFactory->configService->getAsInt("minCountOfObservedAsTopCluster");
        pixelClusterSelector->pixelHitRecencyInSecond = beanFactory->configService->getAsInt("pixelHitRecencyInSecond");
        pixelClusterSelector->pixelCacheService = beanFactory->pixelCacheService.get();
        pixelClusterSelector->pixelDeviceHistoryCassandraService = beanFactory->pixelDeviceHistoryCassandraService.get();

        pixelClusterSelector->minThresholdPercentToChooseTopClusterForIabCat =
                beanFactory->configService->getAsDouble("minThresholdPercentToChooseTopClusterForIabCat");

        clusterer->matrixPopulator = matrixPopulator.get();

        clusterer->
        numberOfClustersToFind =
                beanFactory->configService->getAsInt("startingNumberOfClustersToFind");
        clusterer->
        limitOfRowsInFeatureMatrix =
                beanFactory->configService->getAsInt("limitOfRowsInFeatureMatrix");
        clusterer->
        maxIterationForMatrixConversion =
                beanFactory->configService->getAsInt("maxIterationForMatrixConversion");
        clusterer->trainClusterWithMockData =
                beanFactory->configService->getAsBooleanFromString("trainClusterWithMockData");

        std::string matrixConversionType =
                beanFactory->configService->get("matrixConversionType");
        if (StringUtil::equalsIgnoreCase(matrixConversionType, "DIAG")) {
                clusterer->conversionType = shogun::DIAG;
        } else if  (StringUtil::equalsIgnoreCase(matrixConversionType, "FULL")) {
                clusterer->conversionType = shogun::FULL;
        } else if (StringUtil::equalsIgnoreCase(matrixConversionType, "SPHERICAL")) {
                clusterer->conversionType = shogun::SPHERICAL;
        } else {
                throwEx("wrong matrixConversionType : " + matrixConversionType);
        }

        clusterIndexDescriber->lowerThresholdForPickingCluster =
                beanFactory->configService->getAsDouble("lowerThresholdForPickingCluster");
        clusterIndexDescriber->higherThresholdForPickingCluster =
                beanFactory->configService->getAsDouble("higherThresholdForPickingCluster");
        clusterIndexDescriber->matrixPopulator = matrixPopulator.get();

        clusterer->clusterIndexDescriber = clusterIndexDescriber.get();
        clusterer->dataFetcher = dataFetcher.get();
        clusterer->pixelClusterSelector = pixelClusterSelector.get();
        //we just want to load pixel
        serviceFactory->dataReloadService->entitiesToReload.push_back(Pixel::getEntityName());
        serviceFactory->dataReloadService->reloadDataViaHttp();

        clusterer->process(context);
        exit(0);
        //end of running clustere

}
