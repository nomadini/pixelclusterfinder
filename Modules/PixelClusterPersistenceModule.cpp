#include "PixelClusterPersistenceModule.h"
#include <shogun/clustering/Hierarchical.h>
#include "ModelUtil.h"
#include "StringUtil.h"
#include "GUtil.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "CollectionUtil.h"
#include "PixelCacheService.h"
#include "JsonMapUtil.h"
#include "Pixel.h"
#include "Feature.h"
#include "ClusterContext.h"
#include "JsonArrayUtil.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "LikelihoodToIabCatConverter.h"
#include "MySqlPixelClusterResultService.h"
#include "PixelDeviceHistoryCassandraService.h"

#include <set>
#include <shogun/distance/EuclideanDistance.h>
#include <shogun/features/SparseFeatures.h>
#include <shogun/lib/config.h>
#include <shogun/io/SGIO.h>

PixelClusterPersistenceModule::PixelClusterPersistenceModule() {

}

void PixelClusterPersistenceModule::process(std::shared_ptr<ClusterContext> context) {
        for(auto pixelClusterResult : context->pixelClusterResults) {
                mySqlPixelClusterResultService->insert(pixelClusterResult);
        }
}

PixelClusterPersistenceModule::~PixelClusterPersistenceModule() {
}
