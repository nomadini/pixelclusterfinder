#include "LikelihoodToIabCatConverter.h"
#include <shogun/clustering/Hierarchical.h>
#include "ModelUtil.h"
#include "StringUtil.h"
#include "GUtil.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "CollectionUtil.h"
#include "PixelCacheService.h"
#include "JsonMapUtil.h"
#include "Pixel.h"
#include "Feature.h"
#include "ClusterContext.h"
#include "JsonArrayUtil.h"
#include "DeviceFeatureHistoryCassandraService.h"

#include <set>
#include <shogun/distance/EuclideanDistance.h>
#include <shogun/features/SparseFeatures.h>
#include <shogun/lib/config.h>
#include <shogun/io/SGIO.h>

LikelihoodToIabCatConverter::LikelihoodToIabCatConverter() {
        maxNumberOfHistoryPerDevice = 10;
        featureRecencyInSecond = 24 * 3600; //1 day
        positiveValueInMatrix = 1.0;
        negativeValueInMatrix = 0;
        lowerThresholdForPickingCluster = 30;
        minThresholdPercentToChooseTopClusterForIabCat = 60;
        minCountOfObservedAsTopCluster = 30;
}


std::set<std::string> LikelihoodToIabCatConverter::getTopIabCatsForDevice(
        std::shared_ptr<ClusterContext> context,
        std::string pixelId,
        std::shared_ptr<Device> device,
        int& numberOfUnrelatedDevices,
        int& numberOfRelatedDevices) {
        std::set<std::string> iabCats;
        std::shared_ptr<DeviceFeatureHistory> dfh =
                deviceFeatureHistoryCassandraService->readFeatureHistoryOfDevice(
                        device,
                        featureRecencyInSecond,
                        context->gtldFeatures,
                        maxNumberOfHistoryPerDevice);

        bool atLeastOneFeatureIsCommon = false;
        SGVector<float64_t> dfhVector =
                getFeatureVectorOfDevice(
                        dfh,
                        context->featureIndexMap,
                        atLeastOneFeatureIsCommon);
        if (atLeastOneFeatureIsCommon) {
                MLOG(3) << "dfhVector :  "
                        << ModelUtil::vectorToJson(dfhVector);

                SGVector< float64_t > maximumLikelihoodOfCluster = context->gmm->cluster(dfhVector);
                SGVector<float64_t> sampleVector = context->gmm->sample();
                MLOG(3)<< "sampleVector : " << ModelUtil::vectorToJson(sampleVector);

                iabCats =
                        chooseTopIabCatsBasedOnLikelihoodsForDevice(
                                maximumLikelihoodOfCluster,
                                context->clusterIndexToIabCatToClusterResultMap,
                                context->numberOfTotalDevicesUsed);
                MLOG(2) << "iabCats picked for pixel id : "
                        <<pixelId
                        <<", maximumLikelihoodOfCluster"
                        << ModelUtil::vectorToJson(maximumLikelihoodOfCluster)
                        <<" , are : "
                        << JsonArrayUtil::convertListToJson(iabCats);


                numberOfRelatedDevices++;
        } else {
                numberOfUnrelatedDevices++;
        }
        return iabCats;
}

int LikelihoodToIabCatConverter::getTopClusterIndexOfDevice(
        SGVector<float64_t> maximumLikelihoodOfClusters)
{
        int maxClusterIndex = 0;
        float64_t maxClusterValue = 0;

        for (int clusterIndex = 0;
             clusterIndex < maximumLikelihoodOfClusters.size() - 1;
             clusterIndex++) {

                float64_t maximumLikelihoodOfCluster =
                        maximumLikelihoodOfClusters.get_element(clusterIndex);
                MLOG(1)<<" maximumLikelihoodOfCluster : "
                       << maximumLikelihoodOfCluster
                       << " for cluster " << clusterIndex;

                if (maximumLikelihoodOfCluster > maxClusterValue ) {
                        maxClusterValue = maximumLikelihoodOfCluster;
                        maxClusterIndex = clusterIndex;
                }
        }

        return maxClusterIndex;
}

std::set<std::string> LikelihoodToIabCatConverter::chooseTopIabCatsBasedOnLikelihoodsForDevice(
        SGVector<float64_t> maximumLikelihoodOfClusters,
        std::unordered_map<int,
                           std::shared_ptr<
                                   std::unordered_map<
                                           std::string, std::shared_ptr<ClusterResult>
                                           > > >& clusterIndexToIabCatToClusterResultMap,
        int numberOfTotalDevicesUsed) {

        MLOG(2) <<"maximumLikelihoodOfClusters : "
                << ModelUtil::vectorToJson(maximumLikelihoodOfClusters);

        std::set<std::string> allIabCats;
        int topClusterIndex = getTopClusterIndexOfDevice(maximumLikelihoodOfClusters);

        auto iabCatsFoundMap =
                clusterIndexToIabCatToClusterResultMap.find(topClusterIndex);

        if (iabCatsFoundMap != clusterIndexToIabCatToClusterResultMap.end()) {
                for (auto clusterResultPair : *(iabCatsFoundMap->second)) {
                        if (clusterResultPair.second->countOfObserved <= minCountOfObservedAsTopCluster) {
                                MLOG(2)<<"ignoring cluster because low countOfObserved "<< clusterResultPair.second->toJson()
                                       <<" , minCountOfObservedAsTopCluster : "<< minCountOfObservedAsTopCluster;
                                continue;
                        }
                        double averagePercentageOfCluster =
                                (100 * clusterResultPair.second->countOfObserved) /
                                clusterResultPair.second->maxCountPossible;

                        if (averagePercentageOfCluster >= minThresholdPercentToChooseTopClusterForIabCat) {
                                LOG_EVERY_N(INFO, 10000)<< " choosing "<<clusterResultPair.second->cluster
                                                        << " as topCluster because of averagePercentageOfCluster "
                                                        << averagePercentageOfCluster
                                                        <<", countOfObserved :"<<clusterResultPair.second->countOfObserved
                                                        <<", maxCountPossible :"<<clusterResultPair.second->maxCountPossible;
                                allIabCats.insert(clusterResultPair.second->cluster);
                        } else {
                                LOG_EVERY_N(WARNING, 10000) << google::COUNTER<<"th skipping cluster "
                                                            <<clusterResultPair.second->cluster
                                                            << " because low percentage : "
                                                            <<averagePercentageOfCluster;
                        }
                }
        }

        return allIabCats;
}

SGVector<float64_t> LikelihoodToIabCatConverter::getFeatureVectorOfDevice(
        std::shared_ptr<DeviceFeatureHistory> dfh,
        std::unordered_map<std::string, int> featureIndexMap,
        bool& atLeastOneFeatureIsCommon) {

        std::vector<int> allIndexes =
                ModelUtil::getAllFeatureIndexesInDeviceFeatureHistory(
                        dfh,
                        featureIndexMap);
        MLOG(3)<<" allIndexes of device feature history found in featureIndexMap" <<
                JsonArrayUtil::convertListToJson(allIndexes);
        SGVector<float64_t> dfhVector(featureIndexMap.size(), false);

        for (int32_t i = 0; i < featureIndexMap.size(); i++) {
                if (CollectionUtil::valueExistInList(allIndexes, i)) {
                        dfhVector.set_element(positiveValueInMatrix, i);
                        atLeastOneFeatureIsCommon = true;
                } else {
                        dfhVector.set_element(negativeValueInMatrix, i);
                }
        }
        return dfhVector;
}

LikelihoodToIabCatConverter::~LikelihoodToIabCatConverter() {
}
