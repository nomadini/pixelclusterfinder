#ifndef ClusterIndexDescriber_H
#define ClusterIndexDescriber_H


#include "Status.h" //this is needed in every ClusterIndexDescriber
#include "ClusterContext.h"
#include "Device.h"
#include "ClusterResult.h"
#include "ClusterDetail.h"
#include "DataFetcher.h"
#include "DeviceFeatureHistory.h"
#include <memory>
#include <vector>
#include <unordered_map>
#include <string>
#include <shogun/lib/common.h>

#include <shogun/clustering/GMM.h>
#define HAVE_LAPACK //this is needed for CGMM
class FeatureDeviceHistoryCassandraService;
class MatrixPopulator;
class DeviceFeatureHistoryCassandraService;
class PixelDeviceHistoryCassandraService;
class EntityToModuleStateStats;
class PixelCacheService;
class FeatureDeviceHistory;
class RandomUtil;

using namespace shogun;
class ClusterIndexDescriber {

private:
SGVector<float64_t> getFeatureVectorOfDevice(
        std::shared_ptr<DeviceFeatureHistory> dfh,
        std::unordered_map<std::string, int> featureIndexMap,
        bool& atLeastOneFeatureIsCommon);

void updateResultsForClusterIndexes(
        std::shared_ptr<ClusterDetail> clusterDetail,
        std::unordered_map<int,
                           std::shared_ptr<
                                   std::unordered_map<
                                           std::string,
                                           std::shared_ptr<ClusterResult> > > >& clusterIndexToIabCatToClusterResultMap);

public:
MatrixPopulator* matrixPopulator;
double lowerThresholdForPickingCluster;
double higherThresholdForPickingCluster;

ClusterIndexDescriber();

virtual void process(std::shared_ptr<ClusterContext> context);
void describeClusters(std::shared_ptr<ClusterContext> context);

std::vector<std::shared_ptr<ClusterDetail> > getTopClusterDetails(
        std::string iabCat,
        std::shared_ptr<DeviceFeatureHistory> dfh,
        std::shared_ptr<CGMM> gmm,
        std::unordered_map<std::string, int> featureIndexMap,
        std::vector<double>& valueOfAllClusters);

virtual ~ClusterIndexDescriber();

};

#endif
