#include "MatrixPopulator.h"
#include <shogun/clustering/Hierarchical.h>
#include "ModelUtil.h"
#include "StringUtil.h"
#include "GUtil.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "CollectionUtil.h"
#include <boost/exception/all.hpp>
#include "PixelCacheService.h"
#include "JsonMapUtil.h"
#include "Pixel.h"
#include "Feature.h"
#include "ClusterContext.h"
#include "RandomUtil.h"
#include "JsonArrayUtil.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "PixelDeviceHistoryCassandraService.h"

#include <set>
#include <shogun/distance/EuclideanDistance.h>
#include <shogun/features/SparseFeatures.h>
#include <shogun/lib/config.h>
#include <shogun/io/SGIO.h>
#include <cmath>

MatrixPopulator::MatrixPopulator() {
        positiveValueInMatrix = 10;
        negativeValueInMatrix = 1;
        populatingStrategy = "SIMPLE";
}

double MatrixPopulator::getPositiveValue(
        std::shared_ptr<DeviceFeatureHistory> df,
        int featureIndex) {
        if (StringUtil::equalsIgnoreCase(populatingStrategy, "SIMPLE")) {
                return positiveValueInMatrix;
        }

        if (StringUtil::equalsIgnoreCase(populatingStrategy, "FEATURE_COUNT")) {
                assertAndThrow(!indexFeatureMap.empty());
                auto featurePair = indexFeatureMap.find(featureIndex);
                assertAndThrow(featurePair != indexFeatureMap.end());
                auto vsPair = df->featureToVisitCounts->find(featurePair->second);
                assertAndThrow(vsPair != df->featureToVisitCounts->end());
                return vsPair->second;
        }
        throwEx("not recognized populatingStrategy : " + populatingStrategy);
}

double MatrixPopulator::getNegativeValue(
        std::shared_ptr<DeviceFeatureHistory> df,
        int featureIndex) {
        if (StringUtil::equalsIgnoreCase(populatingStrategy, "SIMPLE")) {
                return negativeValueInMatrix;
        }
        if (StringUtil::equalsIgnoreCase(populatingStrategy, "FEATURE_COUNT")) {
                return negativeValueInMatrix;
        }

        throwEx("not recognized populatingStrategy : " + populatingStrategy);
}

MatrixPopulator::~MatrixPopulator() {
}
