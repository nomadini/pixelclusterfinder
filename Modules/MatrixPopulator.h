#ifndef MatrixPopulator_H
#define MatrixPopulator_H


#include "Status.h" //this is needed in every MatrixPopulator
#include "ClusterContext.h"
#include "Device.h"
#include "ClusterResult.h"
#include "ClusterDetail.h"
#include "ClusterIndexDescriber.h"
#include "PixelClusterSelector.h"
#include "DataFetcher.h"
#include "DeviceFeatureHistory.h"
#include <memory>
#include <vector>
#include <unordered_map>
#include <string>
#include <shogun/lib/common.h>

#include <shogun/clustering/GMM.h>
#define HAVE_LAPACK //this is needed for CGMM
class FeatureDeviceHistoryCassandraService;
class DeviceFeatureHistoryCassandraService;
class PixelDeviceHistoryCassandraService;
class EntityToModuleStateStats;
class PixelCacheService;
class FeatureDeviceHistory;
class RandomUtil;

using namespace shogun;
class MatrixPopulator {

public:

std::string populatingStrategy;
double positiveValueInMatrix;
double negativeValueInMatrix;
std::unordered_map<int, std::string> indexFeatureMap;

MatrixPopulator();

virtual ~MatrixPopulator();

double getPositiveValue(
        std::shared_ptr<DeviceFeatureHistory> deviceFeatureHistory,
        int featureIndex);

double getNegativeValue(
        std::shared_ptr<DeviceFeatureHistory> deviceFeatureHistory,
        int featureIndex);

};

#endif
