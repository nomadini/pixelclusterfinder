#ifndef DataFetcher_H
#define DataFetcher_H


#include "Status.h" //this is needed in every DataFetcher
#include "ClusterContext.h"
#include "Device.h"
#include "ClusterResult.h"
#include "ClusterDetail.h"
#include "DeviceFeatureHistory.h"
#include <memory>
#include <vector>
#include <unordered_map>
#include <string>
#include <shogun/lib/common.h>

#include <shogun/clustering/GMM.h>
#define HAVE_LAPACK //this is needed for CGMM
class FeatureDeviceHistoryCassandraService;
class DeviceFeatureHistoryCassandraService;
class PixelDeviceHistoryCassandraService;
class EntityToModuleStateStats;
class PixelCacheService;
class FeatureDeviceHistory;
class RandomUtil;

using namespace shogun;

class DataFetcher {

public:

FeatureDeviceHistoryCassandraService* featureDeviceHistoryCassandraService;
DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService;

int maxNumberOfIabCatsToPick;
int featureRecencyInSecond;
int iabCatRecencyToPickInSecond;
int maxNumberOfHistoryPerDevice;

int minTimesOfAppearanceForFeatureToConsiderForClustering;


DataFetcher();

void fetchData(std::shared_ptr<ClusterContext> context);

virtual void process(std::shared_ptr<ClusterContext> context);

std::string getKeyOfCluster(
        std::shared_ptr<FeatureDeviceHistory> featureDeviceHistory);

virtual ~DataFetcher();
};

#endif
