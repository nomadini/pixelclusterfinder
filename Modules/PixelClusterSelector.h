#ifndef PixelClusterSelector_H
#define PixelClusterSelector_H


#include "Status.h" //this is needed in every PixelClusterSelector
#include "ClusterContext.h"
#include "Device.h"
#include "ClusterResult.h"
#include "ClusterDetail.h"
#include "DataFetcher.h"
#include "DeviceFeatureHistory.h"
#include <memory>
#include <vector>
#include <unordered_map>
#include <string>
#include <shogun/lib/common.h>

#include <shogun/clustering/GMM.h>
#define HAVE_LAPACK //this is needed for CGMM
class FeatureDeviceHistoryCassandraService;
class LikelihoodToIabCatConverter;
class DeviceFeatureHistoryCassandraService;
class PixelDeviceHistoryCassandraService;
class EntityToModuleStateStats;
class PixelCacheService;
class FeatureDeviceHistory;
class RandomUtil;

using namespace shogun;
class PixelClusterSelector {

private:

public:

double minThresholdPercentToChooseTopClusterForIabCat;
PixelCacheService* pixelCacheService;
LikelihoodToIabCatConverter* likelihoodToIabCatConverter;
PixelDeviceHistoryCassandraService* pixelDeviceHistoryCassandraService;

int pixelHitRecencyInSecond;
int minCountOfObservedAsTopCluster;

PixelClusterSelector();

virtual void process(std::shared_ptr<ClusterContext> context);
void findTheClusterForPixelDevices(std::shared_ptr<ClusterContext> context);

virtual ~PixelClusterSelector();

void countPixelToIabCats(
        std::string pixelId,
        std::unordered_map<std::string,
                           std::shared_ptr<std::unordered_map<std::string, int> > >&
        pixelToIabCatCountsMap,
        std::set<std::string> iabCats);

void findAndPrintTopIabCatsForPixel(
        std::string pixelId,
        std::shared_ptr<ClusterContext> context);

void printAndAddResultsToContext(
        std::shared_ptr<ClusterContext> context,
        std::string pixelId,
        std::unordered_map<std::string,
                           std::shared_ptr<std::unordered_map<std::string, int> > >&
        pixelToIabCatCountsMap,
        int numberOfTotalDevicesHittingPixel);

};

#endif
