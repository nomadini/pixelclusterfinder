#include "ClusterIndexDescriber.h"
#include <shogun/clustering/Hierarchical.h>
#include "ModelUtil.h"
#include "StringUtil.h"
#include "GUtil.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "CollectionUtil.h"
#include "PixelCacheService.h"
#include "JsonMapUtil.h"
#include "Pixel.h"
#include "MatrixPopulator.h"
#include "Feature.h"
#include "ClusterContext.h"
#include "JsonArrayUtil.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "PixelDeviceHistoryCassandraService.h"

#include <set>
#include <shogun/distance/EuclideanDistance.h>
#include <shogun/features/SparseFeatures.h>
#include <shogun/lib/config.h>
#include <shogun/io/SGIO.h>

ClusterIndexDescriber::ClusterIndexDescriber() {
        lowerThresholdForPickingCluster = 50;
        higherThresholdForPickingCluster = 200;
}

void ClusterIndexDescriber::process(std::shared_ptr<ClusterContext> context) {

        describeClusters(context);
}

/*


 */
void ClusterIndexDescriber::describeClusters(std::shared_ptr<ClusterContext> context) {

        for (auto pair : context->iabCatToDeviceFeatueHistoryMap) {
                context->numberOfTotalDevicesUsed++;
                std::string iabCat = pair.first;
                std::shared_ptr<std::vector<std::shared_ptr<DeviceFeatureHistory> > >
                allDevicesForIabCat
                        = pair.second;
                for (auto dfh : *allDevicesForIabCat) {

                        std::vector<double> valueOfAllClusters;
                        auto allPositiveClusterDetailsForDevice =
                                getTopClusterDetails(
                                        iabCat,
                                        dfh,
                                        context->gmm,
                                        context->featureIndexMap,
                                        valueOfAllClusters);

                        MLOG(3)<<"dfh : "<< dfh->toJson()
                               <<" allPositiveClusterDetailsForDevice are : "
                               << JsonArrayUtil::convertListToJson(allPositiveClusterDetailsForDevice) <<
                                " , \n valueOfAllClusters are : "<<
                                JsonArrayUtil::convertListToJson(valueOfAllClusters);

                        for (auto positiveClusterDetail : allPositiveClusterDetailsForDevice) {
                                updateResultsForClusterIndexes(positiveClusterDetail,
                                                               context->clusterIndexToIabCatToClusterResultMap);
                        }
                }
        }


        LOG(INFO)<<"clusterIndexToIabCatToClusterResultMap size : "<< context->clusterIndexToIabCatToClusterResultMap.size();
        LOG(INFO)<<"content of clusterIndexToIabCatToClusterResultMap";
        int maxCountPossible = context->numberOfTotalDevicesUsed * context->gmm->get_coef().size();
        for (auto pair : context->clusterIndexToIabCatToClusterResultMap) {
                auto clusterIndex = pair.first;
                std::vector<std::shared_ptr<ClusterResult> > results;
                for (auto innerPair : *pair.second) {
                        auto clusterResult = innerPair.second;
                        clusterResult->maxCountPossible = maxCountPossible;
                        results.push_back(clusterResult);
                }
                LOG(INFO)<< "cluster index : "
                         << " , clusterResults : "<<JsonArrayUtil::convertListToJson(results);
        }
}

void ClusterIndexDescriber::updateResultsForClusterIndexes(
        std::shared_ptr<ClusterDetail> positiveClusterDetail,
        std::unordered_map<int,
                           std::shared_ptr<
                                   std::unordered_map<
                                           std::string,
                                           std::shared_ptr<ClusterResult> > > >&
        clusterIndexToIabCatToClusterResultMap) {

        auto pair = clusterIndexToIabCatToClusterResultMap.find(positiveClusterDetail->indexId);
        if (pair == clusterIndexToIabCatToClusterResultMap.end()) {
                auto innerMap =
                        std::make_shared<std::unordered_map<std::string, std::shared_ptr<ClusterResult> > >();

                clusterIndexToIabCatToClusterResultMap.insert(std::make_pair(positiveClusterDetail->indexId, innerMap));
                pair = clusterIndexToIabCatToClusterResultMap.find(positiveClusterDetail->indexId);
        }

        std::shared_ptr<std::unordered_map<std::string, std::shared_ptr<ClusterResult> > >
        clusterToClusterResultMap = pair->second;
        auto innerPair = clusterToClusterResultMap->find(positiveClusterDetail->cluster);
        if (innerPair == clusterToClusterResultMap->end()) {
                auto clusterResult = std::make_shared<ClusterResult>();
                clusterResult->cluster = positiveClusterDetail->cluster;
                clusterResult->clusterType = positiveClusterDetail->clusterType;
                clusterToClusterResultMap->insert(std::make_pair(positiveClusterDetail->cluster, clusterResult));
                innerPair = clusterToClusterResultMap->find(positiveClusterDetail->cluster);
        }

        std::shared_ptr<ClusterResult> clusterResultInMap = innerPair->second;
        clusterResultInMap->countOfObserved++;

        // insert(iabCat);
}

std::vector<std::shared_ptr<ClusterDetail> > ClusterIndexDescriber::getTopClusterDetails(
        std::string iabCat,
        std::shared_ptr<DeviceFeatureHistory> dfh,
        std::shared_ptr<CGMM> gmm,
        std::unordered_map<std::string, int> featureIndexMap,
        std::vector<double>& valueOfAllClusters) {

        std::vector<std::shared_ptr<ClusterDetail> > positiveClusterDetails;

        std::unordered_map<double, int> valueOfClusterToClusterIndexMap;

        bool atLeastOneFeatureIsCommon = false;

        SGVector<float64_t> dfhVector =
                getFeatureVectorOfDevice(
                        dfh,
                        featureIndexMap,
                        atLeastOneFeatureIsCommon);
        MLOG(3)<< "dfhVector : " << ModelUtil::vectorToJson(dfhVector);
        if (atLeastOneFeatureIsCommon) {
                SGVector< float64_t > maximumLikelihoodOfCluster = gmm->cluster(dfhVector);
                for (int i = 0; i < maximumLikelihoodOfCluster.size() - 1; i++) {
                        //featureIndexMap.size() -1  becasue we don't count the last cluster result, because it is not a real cluster
                        float64_t clusterValue = maximumLikelihoodOfCluster.get_element(i);
                        valueOfAllClusters.push_back(clusterValue);
                        valueOfClusterToClusterIndexMap.insert(std::make_pair(clusterValue, i));
                        if (clusterValue >= lowerThresholdForPickingCluster &&
                            clusterValue <= higherThresholdForPickingCluster
                            && std::isnan(clusterValue) == false) {
                                auto clusterDetail =
                                        std::make_shared<ClusterDetail>();
                                clusterDetail->indexId = i;
                                clusterDetail->maxLikelihoodScore = clusterValue;
                                clusterDetail->cluster = iabCat;
                                clusterDetail->clusterType = "IAB_CAT_BASED";
                                clusterDetail->uiScore = clusterValue;

                                positiveClusterDetails.push_back(clusterDetail);
                        }
                }
        } else {
                MLOG(3)<<"skipping unrelated device";
        }

        if (valueOfAllClusters.empty()) {
                return positiveClusterDetails;
        }

        float64_t maxValue = 0;
        int maxIndex = 0;
        std::vector<std::shared_ptr<ClusterDetail> > topClusterDetails;
        for(int i=0; i<positiveClusterDetails.size(); i++) {
                if (positiveClusterDetails.at(i)->maxLikelihoodScore > maxValue) {
                        maxValue = positiveClusterDetails.at(i)->maxLikelihoodScore;
                        maxIndex = i;
                        topClusterDetails.push_back(positiveClusterDetails.at(i));
                }
        }

        // int topClusterIndex = maxIndex;
        // if (positiveClusterDetails.size() > 1) {
        //         for(int i=0; i<positiveClusterDetails.size(); i++) {
        //                 if (i == maxIndex) {
        //                         //ignoing the topCluster
        //                         continue;
        //                 }
        //                 if (positiveClusterDetails.at(i)->maxLikelihoodScore > maxValue) {
        //                         maxValue = positiveClusterDetails.at(i)->maxLikelihoodScore;
        //                         maxIndex = i;
        //                 }
        //         }
        // }
        // int secondTopClusterIndex = maxIndex;
        //

        return topClusterDetails;
}

SGVector<float64_t> ClusterIndexDescriber::getFeatureVectorOfDevice(
        std::shared_ptr<DeviceFeatureHistory> dfh,
        std::unordered_map<std::string, int> featureIndexMap,
        bool& atLeastOneFeatureIsCommon) {

        std::vector<int> allIndexes =
                ModelUtil::getAllFeatureIndexesInDeviceFeatureHistory(
                        dfh,
                        featureIndexMap);
        MLOG(3)<<" allIndexes of device feature history found in featureIndexMap" <<
                JsonArrayUtil::convertListToJson(allIndexes);
        SGVector<float64_t> dfhVector(featureIndexMap.size(), false);

        for (int32_t i = 0; i < featureIndexMap.size(); i++) {
                if (CollectionUtil::valueExistInList(allIndexes, i)) {
                        dfhVector.set_element(matrixPopulator->getPositiveValue(dfh, i), i);
                        atLeastOneFeatureIsCommon = true;
                } else {
                        dfhVector.set_element(matrixPopulator->getNegativeValue(dfh, i), i);
                }
        }
        return dfhVector;
}
ClusterIndexDescriber::~ClusterIndexDescriber() {
}
