#ifndef PixelClusterPersistenceModule_H
#define PixelClusterPersistenceModule_H

#include "ClusterContext.h"
#include "Device.h"
#include "ClusterResult.h"
#include "ClusterDetail.h"
#include "DataFetcher.h"
#include "DeviceFeatureHistory.h"
#include <memory>
#include <vector>
#include <unordered_map>
#include <string>
class MySqlPixelClusterResultService;

class PixelClusterPersistenceModule {

private:

public:

MySqlPixelClusterResultService* mySqlPixelClusterResultService;
PixelClusterPersistenceModule();

virtual void process(std::shared_ptr<ClusterContext> context);

virtual ~PixelClusterPersistenceModule();

};

#endif
