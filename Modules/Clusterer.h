#ifndef Clusterer_H
#define Clusterer_H


#include "Status.h" //this is needed in every Clusterer
#include "ClusterContext.h"
#include "Device.h"
#include "ClusterResult.h"
#include "ClusterDetail.h"
#include "ClusterIndexDescriber.h"
#include "PixelClusterSelector.h"
#include "DataFetcher.h"
#include "DeviceFeatureHistory.h"
#include <memory>
#include <vector>
#include <unordered_map>
#include <string>
#include <shogun/lib/common.h>

#include <shogun/clustering/GMM.h>
#define HAVE_LAPACK //this is needed for CGMM
class FeatureDeviceHistoryCassandraService;
class DeviceFeatureHistoryCassandraService;
class PixelDeviceHistoryCassandraService;
class PixelDeviceHistoryCassandraService;
class MatrixPopulator;
class PixelCacheService;
class FeatureDeviceHistory;
class PixelClusterPersistenceModule;
class RandomUtil;

using namespace shogun;
class Clusterer {

public:
static std::string CLUSTER_TYPE_FEATURE_NAME;
static std::string CLUSTER_TYPE_FEATURE_TYPE_IAB_CAT;
static std::string CLUSTER_TYPE_FEATURE_TYPE_IAB_CAT_AND_SUB_CAT;

DataFetcher* dataFetcher;
ClusterIndexDescriber* clusterIndexDescriber;
PixelClusterSelector* pixelClusterSelector;
MatrixPopulator* matrixPopulator;
PixelClusterPersistenceModule* pixelClusterPersistenceModule;

int limitOfRowsInFeatureMatrix;
int maxIterationForMatrixConversion;

int numberOfMockFeatures;
int numberOfMockClusters;
int numberOfClustersToFind;
bool trainClusterWithMockData;
bool inRetryingMode;
ECovType conversionType;

std::shared_ptr<RandomUtil> randomizer;
Clusterer();

virtual void process(std::shared_ptr<ClusterContext> context);
void fetchAndTrainCluster(std::shared_ptr<ClusterContext> context);
void buildCluster(std::shared_ptr<ClusterContext> context);

virtual ~Clusterer();

SGMatrix<float64_t> buildFeatureMatrix(
        std::unordered_map<std::string, int> featureIndexMap,
        std::vector<std::shared_ptr<DeviceFeatureHistory> > deviceFeatureHistoriesInLastDay);

std::shared_ptr<CGMM> trainTheCluster(
        SGMatrix<float64_t> featureMatrix,
        std::shared_ptr<ClusterContext> context);

SGMatrix<float64_t> getRandomMatrix();

void trainClusterWithMockDataFunc(
        SGMatrix<float64_t> featureMatrix,
        std::shared_ptr<ClusterContext> context);
};

#endif
