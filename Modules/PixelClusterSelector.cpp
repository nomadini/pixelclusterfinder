#include "PixelClusterSelector.h"
#include <shogun/clustering/Hierarchical.h>
#include "ModelUtil.h"
#include "StringUtil.h"
#include "GUtil.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "CollectionUtil.h"
#include "PixelCacheService.h"
#include "JsonMapUtil.h"
#include "Pixel.h"
#include "Feature.h"
#include "ClusterContext.h"
#include "JsonArrayUtil.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "LikelihoodToIabCatConverter.h"
#include "PixelDeviceHistoryCassandraService.h"

#include <set>
#include <shogun/distance/EuclideanDistance.h>
#include <shogun/features/SparseFeatures.h>
#include <shogun/lib/config.h>
#include <shogun/io/SGIO.h>

PixelClusterSelector::PixelClusterSelector() {
        pixelHitRecencyInSecond = 3600;
        minThresholdPercentToChooseTopClusterForIabCat = 60;
        minCountOfObservedAsTopCluster = 30;
}

void PixelClusterSelector::process(std::shared_ptr<ClusterContext> context) {

        findTheClusterForPixelDevices(context);
}


void PixelClusterSelector::findTheClusterForPixelDevices(std::shared_ptr<ClusterContext> context) {

        auto allPixels = pixelCacheService->getAllEntities();
        LOG(INFO)<<"allPixels size : "<< allPixels->size();

        for (std::shared_ptr<Pixel> pixel : *allPixels) {
                findAndPrintTopIabCatsForPixel(
                        pixel->uniqueKey,
                        context);
        }
}


void PixelClusterSelector::findAndPrintTopIabCatsForPixel(
        std::string pixelId,
        std::shared_ptr<ClusterContext> context) {

        MLOG(2)<<"finding top iab cats for pixelId : "<< pixelId;

        std::unordered_map<std::string,
                           std::shared_ptr<std::unordered_map<std::string, int> > >
        pixelToIabCatCountsMap;

        auto pixelDeviceHistories =
                pixelDeviceHistoryCassandraService->readDeviceHistoryOfPixel(pixelId,
                                                                             pixelHitRecencyInSecond);
        MLOG(2)<< pixelDeviceHistories->devicesHittingPixel->size()
               <<" devicesHittingPixel fetched for pixelId :"
               << pixelId;

        pixelToIabCatCountsMap.insert(
                std::make_pair(
                        pixelId,
                        std::make_shared<std::unordered_map<std::string, int> > ()));
        int numberOfUnrelatedDevices = 0;
        int numberOfRelatedDevices = 0;

        for (auto&& devicePair : *pixelDeviceHistories->devicesHittingPixel) {
                auto iabCats =
                        likelihoodToIabCatConverter->getTopIabCatsForDevice(
                                context,
                                pixelId,
                                devicePair.second,
                                numberOfUnrelatedDevices,
                                numberOfRelatedDevices);

                countPixelToIabCats(pixelId,
                                    pixelToIabCatCountsMap,
                                    iabCats);
        }

        LOG(INFO)<<" skipping "<<numberOfUnrelatedDevices <<" unrelated devices";
        LOG(INFO)<<" found clusters for "<<numberOfRelatedDevices <<" related devices";
        printAndAddResultsToContext(
                context,
                pixelId,
                pixelToIabCatCountsMap,
                pixelDeviceHistories->devicesHittingPixel->size());
}

void PixelClusterSelector::printAndAddResultsToContext(
        std::shared_ptr<ClusterContext> context,
        std::string pixelId,
        std::unordered_map<std::string,
                           std::shared_ptr<std::unordered_map<std::string, int> > >&
        pixelToIabCatCountsMap,
        int numberOfTotalDevicesHittingPixel) {
        if (pixelToIabCatCountsMap.empty()) {
                LOG(INFO) << " pixelToIabCatCountsMap is empty for pixelId : "<<pixelId;
                return;
        }
        LOG(INFO) << " These are top iab cats for pixelId : "<<pixelId;
        for (auto pair :  pixelToIabCatCountsMap) {
                std::string pixelKey = pair.first;
                LOG(INFO) << "pixel : " << pixelKey;
                for (auto innerPair : *pair.second) {
                        double percent = (innerPair.second * 100 ) / numberOfTotalDevicesHittingPixel;
                        int count = (innerPair.second);
                        numberOfTotalDevicesHittingPixel;
                        std::string cluster = innerPair.first;
                        LOG(INFO)<<
                                " cluster  : "
                                 <<innerPair.first
                                 <<", count : "
                                 <<innerPair.second
                                 <<", numberOfTotalDevicesHittingPixel : "
                                 <<numberOfTotalDevicesHittingPixel
                                 <<", percent of allDevices : "
                                 <<percent;
                        auto pixelClusterResult =
                                std::make_shared<PixelClusterResult>();
                        pixelClusterResult->cluster = cluster;
                        pixelClusterResult->clusterType = "IAB_CAT_BASED";
                        pixelClusterResult->score = percent;
                        pixelClusterResult->scoreType = "PERCENTAGE";
                        pixelClusterResult->pixelId =
                                pixelCacheService->getPixelByKey(pixelKey)->id;

                        context->pixelClusterResults.push_back(pixelClusterResult);
                }
        }
}

void PixelClusterSelector::countPixelToIabCats(
        std::string pixelId,
        std::unordered_map<std::string,
                           std::shared_ptr<std::unordered_map<std::string, int> > >&
        pixelToIabCatCountsMap,
        std::set<std::string> iabCats) {

        auto pair = pixelToIabCatCountsMap.find(pixelId);
        MLOG(3)<<"iab counts to count up "<<
                JsonArrayUtil::convertSetToJson(iabCats)<<
                " , pixelId = "<< pixelId;
        for (auto iabCat : iabCats) {
                auto iabCatCountsMap = pair->second;
                auto iabCatCountsMapPair = iabCatCountsMap->find(iabCat);
                if (iabCatCountsMapPair == iabCatCountsMap->end()) {
                        iabCatCountsMap->insert(std::make_pair(iabCat, 1));
                } else {
                        iabCatCountsMapPair->second++;
                }
        }

}

PixelClusterSelector::~PixelClusterSelector() {
}
