// #ifndef OptimumClusterNumberFinder_H
// #define OptimumClusterNumberFinder_H
//
//
// #include "Status.h" //this is needed in every OptimumClusterNumberFinder
// #include "ClusterContext.h"
// #include "Device.h"
// #include "ClusterResult.h"
// #include "ClusterDetail.h"
// #include "ClusterIndexDescriber.h"
// #include "PixelClusterSelector.h"
// #include "DataFetcher.h"
// #include "DeviceFeatureHistory.h"
// #include <memory>
// #include <vector>
// #include <unordered_map>
// #include <string>
// #include <shogun/lib/common.h>
//
// #include <shogun/clustering/GMM.h>
// #define HAVE_LAPACK //this is needed for CGMM
// class FeatureDeviceHistoryCassandraService;
// class DeviceFeatureHistoryCassandraService;
// class PixelDeviceHistoryCassandraService;
// class EntityToModuleStateStats;
// class PixelCacheService;
// class FeatureDeviceHistory;
// class RandomUtil;
//
// using namespace shogun;
// class OptimumClusterNumberFinder {
//
// public:
// bool lastClusteringTryWasSuccessful;
//
// bool inRetryingMode;
// ECovType conversionType;
//
// std::shared_ptr<RandomUtil> randomizer;
// OptimumClusterNumberFinder();
//
// void findClusterNumber(std::shared_ptr<ClusterContext> context);
//
// virtual ~OptimumClusterNumberFinder();
// };
//
// #endif
