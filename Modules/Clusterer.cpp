#include "Clusterer.h"
#include <shogun/clustering/Hierarchical.h>
#include "ModelUtil.h"
#include "StringUtil.h"
#include "GUtil.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "CollectionUtil.h"
#include <boost/exception/all.hpp>
#include "PixelCacheService.h"
#include "JsonMapUtil.h"
#include "Pixel.h"
#include "Feature.h"
#include "ClusterContext.h"
#include "RandomUtil.h"
#include "PixelClusterPersistenceModule.h"
#include "JsonArrayUtil.h"
#include "MatrixPopulator.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "PixelDeviceHistoryCassandraService.h"

#include <set>
#include <shogun/distance/EuclideanDistance.h>
#include <shogun/features/SparseFeatures.h>
#include <shogun/lib/config.h>
#include <shogun/io/SGIO.h>
#include <cmath>

std::string Clusterer::CLUSTER_TYPE_FEATURE_NAME = "name";
std::string Clusterer::CLUSTER_TYPE_FEATURE_TYPE_IAB_CAT = "iab_cat";
std::string Clusterer::CLUSTER_TYPE_FEATURE_TYPE_IAB_CAT_AND_SUB_CAT = "iab_cat_sub_cat";

Clusterer::Clusterer() {
        limitOfRowsInFeatureMatrix = 100;
        maxIterationForMatrixConversion = 1000;
        trainClusterWithMockData = false;
        inRetryingMode = true;

        numberOfMockFeatures = 10;
        numberOfMockClusters = 2;
        randomizer = std::make_shared<RandomUtil>(2);
        numberOfClustersToFind = 2;
}


//get all iab cat devices histories in last day.
//train the clusters.
//run each iab cat device set through model and see
//how many of devices belong to cluster 1 thru n.
//then you know cluster 1 belongs to iab1 and iab2 for example and etc

//test and find the cluster log log_likelihoods for pixel device histories in last day
//you will find out each device belongs to what cluster and then the iab categories for that cluster

//you will save the pixel to  iab cat  percentages (number of devices belong to that iab cat  over total devices hitting pixel)
void Clusterer::process(std::shared_ptr<ClusterContext> context) {
        do {
                try {
                        buildCluster(context);
                        LOG(INFO)<<"retrying to build cluster in 2 seconds";
                        gicapods::Util::sleepViaBoost(_L_, 2);

                } catch (std::exception const &e) {
                        LOG(ERROR)<<"error happening when handling request  "<< boost::diagnostic_information (e);
                }
        } while(inRetryingMode == true);


        clusterIndexDescriber->process(context);
        pixelClusterSelector->findTheClusterForPixelDevices(context);
        pixelClusterPersistenceModule->process(context);
}

void Clusterer::buildCluster(std::shared_ptr<ClusterContext> context) {

        dataFetcher->fetchData(context);
        fetchAndTrainCluster(context);
}

void Clusterer::fetchAndTrainCluster(std::shared_ptr<ClusterContext> context) {
        SGMatrix<float64_t> featureMatrix;
        //training cluster
        LOG(INFO)<<"train the model with "<<numberOfClustersToFind << " clusters";

        if (trainClusterWithMockData) {
                trainClusterWithMockDataFunc(featureMatrix, context);
        } else {
                featureMatrix =
                        buildFeatureMatrix(context->featureIndexMap,
                                           context->deviceFeatureHistoriesInLastDay);
                try {

                        auto gmm = trainTheCluster(featureMatrix, context);
                        LOG(INFO)<<"done train the cluster co-eficients are : " <<
                                ModelUtil::vectorToJson(gmm->get_coef());
                        auto vector = gmm->get_coef();
                        int numberOfUnknownClusters = 0;
                        for (int i = 0; i< vector.size(); i++) {
                                float64_t var = vector.get_element(i);
                                if (var <= 0 || std::isnan(var)) {
                                        numberOfUnknownClusters++;
                                }
                        }

                        if (numberOfUnknownClusters != 0 &&
                            numberOfUnknownClusters >= vector.size() - 4) {
                                //this is a bad clusterer with a lot of unknown clusters
                                LOG(INFO)<<"model was not built, clusters are bad : numberOfUnknownClusters " <<numberOfUnknownClusters;
                                gmm->cleanup();

                                if (context->gmm != nullptr) {
                                        //last model was good so we stop retrying
                                        LOG(INFO)<<"last model was good, we are done building ";
                                        inRetryingMode = false;
                                } else if (numberOfClustersToFind > 15 ) {
                                        LOG(INFO)<<"no hope to find good model with higher cluster we are done building ";
                                }
                                return;
                        } else {
                                LOG(INFO)<< "model with "
                                         << numberOfClustersToFind
                                         << " clusters was built. numberOfUnknownClusters "
                                         << numberOfUnknownClusters
                                         << ", retrying with higher cluster";
                                context->gmm = gmm;
                                numberOfClustersToFind++;
                                //TODO remove the line after testing
                                inRetryingMode  = false;

                        }
                } catch (shogun::ShogunException &e) {
                        LOG(INFO)<<"exception by shogun : "<< e.get_exception_string ();
                }
        }

}

SGMatrix<float64_t> Clusterer::getRandomMatrix() {

        int num_samples = limitOfRowsInFeatureMatrix;
        int dim_feat = numberOfMockFeatures;
        SGMatrix<float64_t> f_feats_train =
                SGMatrix<float64_t>(
                        dim_feat, num_samples);

        auto clusterToRow =
                std::make_shared<std::unordered_map<int, std::string> >();
        for (int32_t i = 0; i < num_samples; i++) {

                for (int32_t j = 0; j < dim_feat; j++) {
                        if (i % 2 == 1 && j % 2 == 1) {
                                f_feats_train(j, i) = 1;
                        } else {
                                f_feats_train(j, i) = 0;
                        }
                }
        }

        for (int32_t i = 0; i < num_samples; i++) {

                std::string rowStr;
                for (int32_t j = 0; j < dim_feat; j++) {
                        rowStr += _toStr(f_feats_train(j, i));
                }
                LOG(INFO)<<"sample "<<i<<": "<< rowStr;

                clusterToRow->insert(std::make_pair(i, rowStr));
        }

        return f_feats_train;
}

std::shared_ptr<CGMM> Clusterer::trainTheCluster(
        SGMatrix<float64_t> featureMatrix,
        std::shared_ptr<ClusterContext> context) {
        auto features_train =
                new CDenseFeatures<float64_t> (featureMatrix);
        std::shared_ptr<CGMM> gmm = std::make_shared<CGMM>(
                numberOfClustersToFind, conversionType);
        gmm->set_features(features_train);

        float64_t min_cov=1e-9;
        float64_t min_change=1e-9;
        NULL_CHECK(gmm);
        gmm->train_em(min_cov,
                      maxIterationForMatrixConversion,
                      min_change);


        return gmm;
}

SGMatrix<float64_t> Clusterer::buildFeatureMatrix(
        std::unordered_map<std::string, int> featureIndexMap,
        std::vector<std::shared_ptr<DeviceFeatureHistory> > deviceFeatureHistoriesInLastDay) {
        //building featureMatrix
        int numberOfFeaturesInMatrix = featureIndexMap.size();
        int numberOfRowsInMatrix = deviceFeatureHistoriesInLastDay.size();
        if (numberOfRowsInMatrix > limitOfRowsInFeatureMatrix) {
                numberOfRowsInMatrix = limitOfRowsInFeatureMatrix;
        }
        if (numberOfRowsInMatrix < numberOfFeaturesInMatrix) {
                throwEx("rows are less than # features."
                        + _toStr(numberOfRowsInMatrix) + " vs " +
                        _toStr(numberOfFeaturesInMatrix));
        }
        if (numberOfRowsInMatrix == 0 || numberOfFeaturesInMatrix == 0) {
                throwEx("cannot build any model with 0 data");
        }

        SGMatrix<float64_t> featureMatrix =
                SGMatrix<float64_t>(
                        numberOfFeaturesInMatrix, numberOfRowsInMatrix);

        int numberOfRowsBuildInFeatureMatrix = 0;
        LOG(INFO)<< "building featureMatrix with "
                 << numberOfRowsInMatrix
                 << " rows and "
                 << numberOfFeaturesInMatrix
                 << " columns from max "
                 << numberOfRowsInMatrix
                 << " data samples";

        std::unordered_map<int, std::string> indexFeatureMap =
                ModelUtil::getIndexFeatureMap(featureIndexMap);
        matrixPopulator->indexFeatureMap = indexFeatureMap;

        for (int32_t i = 0; i < numberOfRowsInMatrix; i++) {
                auto deviceHistory = deviceFeatureHistoriesInLastDay.at(i);

                std::vector<int> allIndexes =
                        ModelUtil::getAllFeatureIndexesInDeviceFeatureHistory(
                                deviceHistory,
                                featureIndexMap);

                MLOG(2)<<
                        "allIndexes to build row in featureMatrix : " <<
                        JsonArrayUtil::convertListToJson(allIndexes);

                for (int32_t featureIndex = 0; featureIndex < numberOfFeaturesInMatrix; featureIndex++) {
                        if (CollectionUtil::valueExistInList(allIndexes, featureIndex)) {
                                featureMatrix(featureIndex, i) =
                                        matrixPopulator->getPositiveValue(deviceHistory, featureIndex);
                        } else {
                                featureMatrix(featureIndex, i) =
                                        matrixPopulator->getNegativeValue(deviceHistory, featureIndex);
                        }
                }

                numberOfRowsBuildInFeatureMatrix++;
                if (numberOfRowsBuildInFeatureMatrix >= limitOfRowsInFeatureMatrix) {
                        break;
                }
        }

        // featureMatrix.display_matrix("featureMatrix");

        return featureMatrix;
}

void Clusterer::trainClusterWithMockDataFunc(
        SGMatrix<float64_t> featureMatrix,
        std::shared_ptr<ClusterContext> context) {
        featureMatrix = getRandomMatrix();
        auto features_train =
                new CDenseFeatures<float64_t> (featureMatrix);
        context->gmm = std::make_shared<CGMM>(
                numberOfMockClusters, conversionType);
        context->gmm->set_features(features_train);

        float64_t min_cov=1e-9;
        float64_t min_change=1e-9;
        context->gmm->train_em(min_cov,
                               maxIterationForMatrixConversion,
                               min_change);

        LOG(INFO)<<"done train the cluster co-eficients are : " <<
                ModelUtil::vectorToJson(context->gmm->get_coef());
}

Clusterer::~Clusterer() {
}
