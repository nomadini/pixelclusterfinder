#include "DataFetcher.h"
#include <shogun/clustering/Hierarchical.h>
#include "ModelUtil.h"
#include "StringUtil.h"
#include "GUtil.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "CollectionUtil.h"
#include "PixelCacheService.h"
#include "JsonMapUtil.h"
#include "Pixel.h"
#include "Feature.h"
#include "ClusterContext.h"
#include "JsonArrayUtil.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "PixelDeviceHistoryCassandraService.h"

#include <set>
#include <shogun/distance/EuclideanDistance.h>
#include <shogun/features/SparseFeatures.h>
#include <shogun/lib/config.h>
#include <shogun/io/SGIO.h>

DataFetcher::DataFetcher() {
        iabCatRecencyToPickInSecond = 3600;
        maxNumberOfIabCatsToPick = 20;
        maxNumberOfHistoryPerDevice = 10;
        featureRecencyInSecond = 24 * 3600; //1 day


        minTimesOfAppearanceForFeatureToConsiderForClustering = 1;
}

void DataFetcher::process(std::shared_ptr<ClusterContext> context) {
        fetchData(context);
}

std::string DataFetcher::getKeyOfCluster(
        std::shared_ptr<FeatureDeviceHistory> featureDeviceHistory) {
        return featureDeviceHistory->feature->getName();
}

void DataFetcher::fetchData(std::shared_ptr<ClusterContext> context) {
        std::set<std::string> featureTypesRequested;
        featureTypesRequested.insert(Feature::IAB_CAT);
        std::set<std::string> featuresToExclude;


        std::vector<std::shared_ptr<FeatureDeviceHistory> >
        devicesHittingWithIabCatsLastday =
                featureDeviceHistoryCassandraService->readDistinctFeaturesExcluding(
                        featuresToExclude,
                        maxNumberOfIabCatsToPick,
                        iabCatRecencyToPickInSecond,
                        featureTypesRequested);
        LOG(INFO)<<"devicesHittingWithIabCatsLastday size "<<devicesHittingWithIabCatsLastday.size();

        //TODO : only pick the devices that have hit an iab cat more than x times
        //in order improve the quality of clustering like how you did with modeler
        // devicesHittingWithIabCatsLastday = getMoreQualityDevices(devicesHittingWithIabCatsLastday);
        int numberOfEmptyDeviceFeatureHistories = 0;


        for(auto featureDeviceHistory : devicesHittingWithIabCatsLastday) {

                std::string keyOfCluster = getKeyOfCluster(featureDeviceHistory);


                MLOG(3)<<"featureDeviceHistory->devicehistories size : "<<
                        featureDeviceHistory->devicehistories.size();

                for (auto deviceHistory : featureDeviceHistory->devicehistories) {
                        std::shared_ptr<DeviceFeatureHistory>  rawDfh =
                                deviceFeatureHistoryCassandraService->readFeatureHistoryOfDevice(
                                        deviceHistory->device,
                                        featureRecencyInSecond,
                                        context->gtldFeatures,
                                        maxNumberOfHistoryPerDevice);

                        std::shared_ptr<DeviceFeatureHistory> dfh =
                                rawDfh->getCleanHistoryClone(
                                        minTimesOfAppearanceForFeatureToConsiderForClustering);
                        dfh->consutructFeatureVisitCountsMap();

                        if (dfh->getFeatures()->empty()) {
                                numberOfEmptyDeviceFeatureHistories++;
                                continue;
                        }


                        //populating all device feature history array
                        MLOG(2)<<"dfh features size "<<dfh->getFeatures()->size();
                        context->deviceFeatureHistoriesInLastDay.push_back(dfh);


                        //populating map of iabCat to deviceFeatureHistories
                        auto pair = context->iabCatToDeviceFeatueHistoryMap.find(
                                keyOfCluster
                                );
                        if (pair == context->iabCatToDeviceFeatueHistoryMap.end()) {
                                auto vec = std::make_shared<std::vector<std::shared_ptr<DeviceFeatureHistory> > >();
                                vec->push_back(dfh);
                                context->iabCatToDeviceFeatueHistoryMap.insert(
                                        std::make_pair(
                                                keyOfCluster, vec
                                                ));
                        } else {
                                pair->second->push_back(dfh);
                        }
                }
        }

        LOG(INFO)<<"# of empty dfh features histories "<< numberOfEmptyDeviceFeatureHistories;
        MLOG(3)<<"iabCatToDeviceFeatueHistoryMap size is "<< context->iabCatToDeviceFeatueHistoryMap.size();
        MLOG(3)<<"deviceFeatureHistoriesInLastDay to cluster data based on  "<<
                context->deviceFeatureHistoriesInLastDay.size();


        //populating featureIndexMap
        for (auto oneDeviceHistory : context->deviceFeatureHistoriesInLastDay) {
                std::set<std::string> allFeatureSet =
                        ModelUtil::getFeatureSetFromDeviceHistories(oneDeviceHistory, context->gtldFeatures);
                ModelUtil::updateFeatureIndexMapWithNewFeatures(allFeatureSet, context->featureIndexMap);
        }

        LOG(INFO)<<"featureIndexMap size is "
                 << context->featureIndexMap.size()
                 <<" featureIndexMap : "
                 << JsonMapUtil::convertMapToJsonArray(context->featureIndexMap);

}

DataFetcher::~DataFetcher() {
}
