#ifndef LikelihoodToIabCatConverter_H
#define LikelihoodToIabCatConverter_H


#include "Status.h" //this is needed in every LikelihoodToIabCatConverter
#include "ClusterContext.h"
#include "Device.h"
#include "ClusterResult.h"
#include "ClusterDetail.h"
#include "DataFetcher.h"
#include "DeviceFeatureHistory.h"
#include <memory>
#include <vector>
#include <unordered_map>
#include <string>
#include <shogun/lib/common.h>

#include <shogun/clustering/GMM.h>
#define HAVE_LAPACK //this is needed for CGMM
class FeatureDeviceHistoryCassandraService;
class DeviceFeatureHistoryCassandraService;
class PixelDeviceHistoryCassandraService;
class EntityToModuleStateStats;
class PixelCacheService;
class FeatureDeviceHistory;
class RandomUtil;

using namespace shogun;
class LikelihoodToIabCatConverter {

public:
double positiveValueInMatrix;
double negativeValueInMatrix;
double lowerThresholdForPickingCluster;
double minThresholdPercentToChooseTopClusterForIabCat;
int minCountOfObservedAsTopCluster;
int maxNumberOfHistoryPerDevice;
int featureRecencyInSecond;

DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService;
LikelihoodToIabCatConverter();

virtual ~LikelihoodToIabCatConverter();

std::set<std::string> getTopIabCatsForDevice(
        std::shared_ptr<ClusterContext> context,
        std::string pixelId,
        std::shared_ptr<Device> device,
        int& numberOfUnrelatedDevices,
        int& numberOfRelatedDevices);

std::set<std::string> chooseTopIabCatsBasedOnLikelihoodsForDevice(
        SGVector<float64_t> maximumLikelihoodOfClusters,
        std::unordered_map<int,
                           std::shared_ptr<
                                   std::unordered_map<
                                           std::string, std::shared_ptr<ClusterResult>
                                           > > >& clusterIndexToIabCatToClusterResultMap,
        int numberOfTotalDevicesUsed);

SGVector<float64_t> getFeatureVectorOfDevice(
        std::shared_ptr<DeviceFeatureHistory> dfh,
        std::unordered_map<std::string, int> featureIndexMap,
        bool& atLeastOneFeatureIsCommon);

int getTopClusterIndexOfDevice(
        SGVector<float64_t> maximumLikelihoodOfClusters);


};

#endif
