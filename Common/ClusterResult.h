#ifndef ClusterResult_H
#define ClusterResult_H


#include "Object.h"
#include <string>

#include "Poco/DateTime.h"
#include "JsonTypeDefs.h"

class ClusterResult : public Object {

private:

public:

int countOfObserved;
int maxCountPossible;
std::string cluster;
std::string clusterType;

ClusterResult();
virtual ~ClusterResult();

static std::shared_ptr<ClusterResult> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);

std::string toJson();

};

#endif
