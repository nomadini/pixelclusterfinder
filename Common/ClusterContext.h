#ifndef ClusterContext_H
#define ClusterContext_H


#include "Object.h"
#include <string>

#include "Poco/DateTime.h"
#include "JsonTypeDefs.h"
#include "ClusterResult.h"
#include "PixelClusterResult.h"
#include "DeviceFeatureHistory.h"
#include <memory>
#include <vector>
#include <unordered_map>
#include <string>
#include <shogun/clustering/GMM.h>
using namespace shogun;
class ClusterContext : public Object {

private:

public:
int numberOfTotalDevicesUsed;
std::shared_ptr<CGMM> gmm;
std::unordered_map<std::string, int> featureIndexMap;
std::unordered_map<std::string,
                   std::shared_ptr<std::vector<std::shared_ptr<DeviceFeatureHistory> > > >
iabCatToDeviceFeatueHistoryMap;
std::vector<std::shared_ptr<DeviceFeatureHistory> > deviceFeatureHistoriesInLastDay;
std::set<std::string> gtldFeatures;

std::unordered_map<int,
                   std::shared_ptr<std::unordered_map<std::string, std::shared_ptr<ClusterResult> > > >
clusterIndexToIabCatToClusterResultMap;

std::vector<std::shared_ptr<PixelClusterResult> > pixelClusterResults;
ClusterContext();
virtual ~ClusterContext();

};

#endif
