#ifndef ClusterDetail_H
#define ClusterDetail_H


#include "Object.h"
#include <string>

#include "Poco/DateTime.h"
#include "JsonTypeDefs.h"

class ClusterDetail : public Object {

private:

public:

int indexId;
double maxLikelihoodScore;

double uiScore;//this score is showned to clients in the UI

std::string cluster;
std::string clusterType;

ClusterDetail();
virtual ~ClusterDetail();

static std::shared_ptr<ClusterDetail> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);

std::string toJson();

};

#endif
