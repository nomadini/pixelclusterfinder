#include "ClusterResult.h"
#include "ModelUtil.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"
#include "StringUtil.h"

ClusterResult::ClusterResult() : Object(__FILE__) {
        maxCountPossible = 0;
        countOfObserved = 0;
}

ClusterResult::~ClusterResult() {
}

std::shared_ptr<ClusterResult> ClusterResult::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<ClusterResult> obj = std::make_shared<ClusterResult>();
        obj->countOfObserved = JsonUtil::GetIntSafely(value, "countOfObserved");
        obj->maxCountPossible = JsonUtil::GetIntSafely(value, "maxCountPossible");
        obj->cluster = JsonUtil::GetStringSafely(value, "cluster");
        obj->clusterType = JsonUtil::GetStringSafely(value, "clusterType");
        return obj;
}

void ClusterResult::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "countOfObserved", countOfObserved, value);
        JsonUtil::addMemberToValue_FromPair (doc, "maxCountPossible", maxCountPossible, value);
        JsonUtil::addMemberToValue_FromPair (doc, "clusterType", clusterType, value);
        JsonUtil::addMemberToValue_FromPair (doc, "cluster", cluster, value);
}

std::string ClusterResult::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);

}
