#include "ClusterDetail.h"
#include "ModelUtil.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"
#include "StringUtil.h"

ClusterDetail::ClusterDetail() : Object(__FILE__) {

        indexId = 0;
        maxLikelihoodScore = 0;
}

ClusterDetail::~ClusterDetail() {
}

std::shared_ptr<ClusterDetail> ClusterDetail::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<ClusterDetail> obj = std::make_shared<ClusterDetail>();
        obj->indexId = JsonUtil::GetIntSafely(value, "indexId");
        obj->maxLikelihoodScore = JsonUtil::GetDoubleSafely(value, "maxLikelihoodScore");
        obj->uiScore = JsonUtil::GetDoubleSafely(value, "uiScore");
        obj->cluster = JsonUtil::GetStringSafely(value, "cluster");
        obj->clusterType = JsonUtil::GetStringSafely(value, "clusterType");

        return obj;
}

void ClusterDetail::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "indexId", indexId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "maxLikelihoodScore",
                                             maxLikelihoodScore, value);
        JsonUtil::addMemberToValue_FromPair (doc, "uiScore",
                                             uiScore, value);
        JsonUtil::addMemberToValue_FromPair (doc, "clusterType", clusterType, value);
        JsonUtil::addMemberToValue_FromPair (doc, "cluster", cluster, value);
}

std::string ClusterDetail::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);

}
