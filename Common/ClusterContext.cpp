#include "ClusterContext.h"
#include "ModelUtil.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"
#include "StringUtil.h"

ClusterContext::ClusterContext() : Object(__FILE__) {
        gtldFeatures.insert(Feature::IAB_CAT);
        numberOfTotalDevicesUsed = 0;
}

ClusterContext::~ClusterContext() {
}
